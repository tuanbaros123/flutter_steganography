# flutter_steganography

`flutter_steganography` is a simple package for fully transparent hiding any string within an image. This technique is known as LSB (Least Significant Bit) [steganography](https://en.wikipedia.org/wiki/steganography) 

## Getting Started

This project is a starting point for a Dart
[package](https://flutter.dev/developing-packages/),
a library module containing code that can be shared easily across
multiple Flutter or Dart projects.

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.

## Usage

### How to encode message into an image?
- Create an `EncodeRequest`
```dart
import 'dart:io';
...

File file = File(...);
// the key is use to encrypt your message with AES256 algorithm
EncodeRequest request = EncodeRequest(file.readAsBytesSync(), "your_message", key: "your_key");
```

- Encode
```dart
// for async
Uint8List response = await encodeMessageIntoImageAsync(request);

//or for sync
Uint8List response = encodeMessageIntoImage(request);

```

- Display encoded image
```dart
import 'package:flutter/material.dart';
...

Image.memory(response);
```

### How to decode message from an image?
- Create a `DecodeRequest`
```dart
import 'dart:io';
...

File file = File(...);
// the key is use to decrypt your encrypted message with AES256 algorithm
DecodeRequest request = DecodeRequest(file.readAsBytesSync(), key: "your_key");
```

- Decode
```dart
// for async
String response = await decodeMessageFromImageAsync(request);

//or for sync
Uint8List response = decodeMessageFromImage(request);
```

- Display message
```dart
import 'package:flutter/material.dart';
...

Text(response);
```

## References
- https://en.wikipedia.org/wiki/Steganography
- https://securitydaily.net/gioi-thieu-ky-thuat-giau-tin-trong-anh-steganography-phan-2/
- https://github.com/tianhaoz95/photochat

