import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_steganography/decoder.dart';
import 'package:flutter_steganography/encoder.dart';
import 'package:flutter_steganography/requests/requests.dart';
import 'package:image_picker/image_picker.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final picker = ImagePicker();
  Uint8List image;
  String text = "Example";
  bool isLoading = false;

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    if (pickedFile != null) {
      setState(() {
        isLoading = true;
      });
      File _image = File(pickedFile.path);
      EncodeRequest request = EncodeRequest(_image.readAsBytesSync(), "your_message", key: "your_key");
      // for async
      image = await encodeMessageIntoImageAsync(request);
      setState(() {
        isLoading = false;
      });
    } else {
      print('No image selected.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(text),
      ),
      body: Center(
        child: isLoading
            ? CircularProgressIndicator()
            : image == null
                ? Text('No image selected.')
                : GestureDetector(
                    child: Image.memory(image),
                    onTap: () async {
                      setState(() {
                        isLoading = true;
                      });
                      DecodeRequest request = DecodeRequest(image, key: "your_key");
                      text = await decodeMessageFromImageAsync(request);
                      setState(() {
                        isLoading = false;
                      });
                    },
                  ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }
}
